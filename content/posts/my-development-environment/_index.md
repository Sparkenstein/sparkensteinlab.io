---
title: My Development Environment!
excerpt: >-
  Here's the list of all hardware and software I use in 2019 for development
date: '2019-11-30 03:14:01'
thumb_img_path: images/7.jpg
content_img_path: images/7.jpg
layout: post
---

Alright, it's been a while since I wrote a post on fosslife. To be honest, it's been a year.
I stopped writing because I was looking for a job change last year, which lasted for like 6 months or something
and then with new job, new responsibilities and stuff.

But anyway, here I am, hopefully will try to write every weekend if time permits. So to start after a long gap 
I have decided not to pick a technical topic this time. Instead I will be listing out my favorite apps and tools
that I use in my daily life for better productivity. Of course this will change in upcoming years but as of today (Dec 2019), this is what I find most comfortable

## Hardware
let's talk about hardware first as it's a small topic. software tools vary from multiple Operating Systems to neovim to fonts so we will cover the tool later.
Currently, I own a single desktop machine of my own and a Laptop provided by my current Organization. 
The Desktop, I call it "VeloCT" is almost 3/4 years old, but still runs flawlessly. I assembled VeloCT myself at my home, and when I bought it, I made sure that I am getting the latest and fastest hardware I could get within my budget (isn't that what we always do?). It runs on a `Intel(R) Core(TM) i5-4690 CPU @ 3.50GHz` CPU overclocked at `3.9 GHz` :smiley: At that time my requirement for RAM was not that high, so I bought a single Corsair 8GB 1600 MHz DDR3 RAM. PS: it has 10 years of warranty.

Here's a deal breaker though :- I don't have a GPU (as of now). Having an external GPU for desktops is a really common thing, but unfortunately I had to decide between
buying a powerful CPU + no GPU _OR_ a mediocre CPU + mediocre GPU (within the budget of course). And for me the choice was easy. So I went for high end CPU and no GPU as I am not a Designer or Gamer anyway (I regret it sometimes though).

For monitors, I have dual monitor setup, primary is 61 cm (24" in retarded) Samsung Curved and secondary is 43 cm (17" in retarded) Dell. and if I am working from home, I sometimes attach my monitors to my laptop too. 

So to list things out here's my hardware
 - Intel(R) Core(TM) i5-4690 CPU @ 3.50GHz
 - Corsair 8GB 1600 MHz DDR3 RAM
 - MSI Z87-G55 Military Class 4 Motherboard
 - Samsung 61 cm Curved + Dell 43 cm
 - Amazon Basic Keyboard + Mouse Combo
 - Maono AU-A04 Condenser Microphone Kit for recording tutorials etc
 - 4/5 different headphones/earphones according to need
 - Arduino and supporting (lots of) electronic components to play around sometimes

And stuff....

## Software
That's a quite huge topic to be honest. Also everyone likes to use what they are most comfortable in. Without describing much I will just list out my development environment

### Editors 
 - VSCode
   - Atom One Dark, Dracula, One Dark Pro and Material Icons theme
   - Vetur for VueJS apps
   - Prettier, ESlint for JS projects
   - Fira Code, Inconsolata
   - RLS and few other Rust tooling
   - Quokka.js for rapid prototyping
   - Gitlens
 - Vim
   - vim-coc

### Langugages
 - Nodejs with [nvs](https://github.com/jasongin/nvs) and Yarn
 - Golang
 - Rust with rustup and cargo
 - Python
 - Clojure with leiningen

### Tools
 - zsh, antigen, oh-my-zsh
 - [starship](https://starship.rs/)
 - rofi
 - flameshot
 - SimpleScreenRecorder


### Environment
 - i3-gaps and other i3 tools
 - xfce4
 - urxvt or kitty