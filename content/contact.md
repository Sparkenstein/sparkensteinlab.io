---
title: Contact
layout: contact
menu:
  main:
    weight: 5
    name: Contact
---

Have a question? Have an awesome product idea? Wanna just say Hi?
Hit me on my any social media handler, and I will make sure to get back to you.

Additionally you can send me mail at <a href="mailto:prabhanjan@fosslife.com?subject=Hello%20Prabhanjan">My email</a> Directly or,
just hit on links below to visit my social media handlers
