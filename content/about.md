---
title: About Me
subtitle: This is a short page about me and my work.
img_path: images/about.jpg
layout: page
menu:
  main:
    weight: 4
    name: About
---

**Prabhanjan Padhye**, Software Developer from [Pune](https://en.wikipedia.org/wiki/Pune), India.

I am a full stack developer, with a 3+ years of industrial experience. I *love* coding, especially in JS. I can talk about [JavaScript](https://www.ecma-international.org/ecma-262/10.0/index.html#Title) and [Rust](https://www.rust-lang.org/) all day.

I am also a spearker/author. I write blogs in my free time, make coding tutorial etc. 
