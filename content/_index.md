---
title: Home
sections:
  - section_id: hero
    component: hero_block.html
    type: heroblock
    content: >-
      Full Stack JavaScript and Rust Developer
  - section_id: about
    component: content_block.html
    type: contentblock
    title: About
    content: >-
      Hi, Myself Prabhanjan. I am a Full Stack Web developer. I love creating apps with
      both JavaScript and Rust. I write blogs/tutorials sometimes and also host services for developers
      Say "Hi!" to me at my any social media handler, If you are in Pune, let's grab a coffee on a weekend!
    actions:
      - label: Contact Me
        url: /contact
  - section_id: recent-posts
    component: posts_block.html
    type: postsblock
    title: Recent Posts
    num_posts_displayed: 4
    actions:
      - label: View Blog
        url: blog/index.html
layout: home
menu:
  main:
    weight: 1
    name: Home
---
